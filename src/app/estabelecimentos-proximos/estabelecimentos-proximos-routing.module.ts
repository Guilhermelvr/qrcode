import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EstabelecimentosProximosPage } from './estabelecimentos-proximos.page';

const routes: Routes = [
  {
    path: '',
    component: EstabelecimentosProximosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EstabelecimentosProximosPageRoutingModule {}
