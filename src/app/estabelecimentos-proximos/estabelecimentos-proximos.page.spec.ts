import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EstabelecimentosProximosPage } from './estabelecimentos-proximos.page';

describe('EstabelecimentosProximosPage', () => {
  let component: EstabelecimentosProximosPage;
  let fixture: ComponentFixture<EstabelecimentosProximosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstabelecimentosProximosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EstabelecimentosProximosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
