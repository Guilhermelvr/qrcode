import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { async } from '@angular/core/testing';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-cadastro-cliente',
  templateUrl: './cadastro-cliente.page.html',
  styleUrls: ['./cadastro-cliente.page.scss'],
})
export class CadastroClientePage {
  [x: string]: any;

  loginForm: any;

  constructor(formBuilder: FormBuilder, public alertController: AlertController) {
    this.loginForm = formBuilder.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      tel: new FormControl('', [Validators.required]),
      senha: new FormControl(['', Validators.compose([Validators.minLength(1), Validators.maxLength(20),
      Validators.required])]),
      senha2: new FormControl (['', Validators.compose([Validators.minLength(1), Validators.maxLength(20),
        Validators.required])]),
    });
   
  }
  async salva() {
    let { email, senha, tel, senha2 } = this.loginForm.controls;

    if (!this.loginForm.valid) {
    }
    if (!email.valid) {
      const alert = await this.alertController.create({
        header: 'Cadastro Cliente',
        
        message: 'Email invalido ou campo vazio',
        buttons: ['OK']
      });
  
      await alert.present();
  
        
    } else if(!senha.valid) {
      const alert = await this.alertController.create({
        header: 'Cadastro Cliente',
        
        message: "Preencha o campo senha",
        buttons: ['OK']
      });
  
      await alert.present();
      
    }else if(!tel.valid) {
      const alert = await this.alertController.create({
        header: 'Cadastro Cliente',
        
        message: 'Preencha o campo telefone',
        buttons: ['OK']
      });
  
      await alert.present();
      
    }else if(!senha2.valid){
      const alert = await this.alertController.create({
        header: 'Cadastro Cliente',
        
        message: "Preencha o campo senha",
        buttons: ['OK']
      });
  
      await alert.present();
    
    }
    else {
       
        const alert = await this.alertController.create({
          header: 'Cadastro Cliente',
          
          message: 'Cadastro Efetuado com sucesso',
          buttons: ['OK']
        });
    
        await alert.present();
      }
    }
  }
  


