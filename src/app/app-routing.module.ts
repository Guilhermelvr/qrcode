import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'cadastro-estabelecimento',
    loadChildren: () => import('./cadastro-estabelecimento/cadastro-estabelecimento.module').then( m => m.CadastroEstabelecimentoPageModule)
  },

  {
    path: 'cadastro-cliente',
    loadChildren: () => import('./cadastro-cliente/cadastro-cliente.module').then( m => m.CadastroClientePageModule)
  },
  {
    path: 'area-estabelecimento',
    loadChildren: () => import('./area-estabelecimento/area-estabelecimento.module').then( m => m.AreaEstabelecimentoPageModule)
  },
  {
    path: 'cadastro-tipo-consumo',
    loadChildren: () => import('./cadastro-tipo-consumo/cadastro-tipo-consumo.module').then( m => m.CadastroTipoConsumoPageModule)
  },
  {
    path: 'form-tipo-consumo',
    loadChildren: () => import('./form-tipo-consumo/form-tipo-consumo.module').then( m => m.FormTipoConsumoPageModule)
  },
  {
    path: 'cadastro-recompensa',
    loadChildren: () => import('./cadastro-recompensa/cadastro-recompensa.module').then( m => m.CadastroRecompensaPageModule)
  },
  {
    path: 'form-recompensa',
    loadChildren: () => import('./form-recompensa/form-recompensa.module').then( m => m.FormRecompensaPageModule)
  },

  {
    path: 'validar-resgate',
    loadChildren: () => import('./validar-resgate/validar-resgate.module').then( m => m.ValidarResgatePageModule)
  },
  {
    path: 'marcar-consumo',
    loadChildren: () => import('./marcar-consumo/marcar-consumo.module').then( m => m.MarcarConsumoPageModule)
  },
  {
    path: 'area-cliente',
    loadChildren: () => import('./area-cliente/area-cliente.module').then( m => m.AreaClientePageModule)
  },
  {
    path: 'estabelecimentos-proximos',
    loadChildren: () => import('./estabelecimentos-proximos/estabelecimentos-proximos.module').then( m => m.EstabelecimentosProximosPageModule)
  },
  
  {
    path: 'historico-consumo',
    loadChildren: () => import('./historico-consumo/historico-consumo.module').then( m => m.HistoricoConsumoPageModule)
  },
  {
    path: 'resgates-disponiveis',
    loadChildren: () => import('./resgates-disponiveis/resgates-disponiveis.module').then( m => m.ResgatesDisponiveisPageModule)
  },
  {
    path: 'generate-qrcode',
    loadChildren: () => import('./generate-qrcode/generate-qrcode.module').then( m => m.GenerateQrcodePageModule)
  },
  {
    path: 'read-qrcode',
    loadChildren: () => import('./read-qrcode/read-qrcode.module').then( m => m.ReadQrcodePageModule)
  },
  {
    path: 'leitorqrcode',
    loadChildren: () => import('./leitorqrcode/leitorqrcode.module').then( m => m.LeitorqrcodePageModule)
  },
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
