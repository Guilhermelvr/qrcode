import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { ReactiveFormsModule } from "@angular/forms";
@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro-estabelecimento.page.html',
  styleUrls: ['./cadastro-estabelecimento.page.scss'],
})
export class CadastroEstabelecimentoPage{
  loginForm: any;

  constructor(formBuilder: FormBuilder) {
    this.loginForm = formBuilder.group({
      email: ['', Validators.required],
      tel: ['', Validators.required],
      estabelecimento: ['', Validators.required],
      seutel: ['', Validators.required],
      password: ['', Validators.compose([Validators.minLength(1), Validators.maxLength(20),
      Validators.required])],
    });
   
  }
  salva() {
    let { email, password, seutel, estabelecimento, tel } = this.loginForm.controls;

    if (!this.loginForm.valid) {
    }
    if (!email.valid) {
        console.log("Preencha o campo email");
        
      } else if(!password.valid) {
        console.log("Preencha o campo senha");
      }
     else if(!seutel.valid) {
      console.log("Preencha o campo telefone");
    }else if(!estabelecimento.valid) {
      console.log("Preencha o campo do estabelecimento");
    }else if(!tel.valid) {
      console.log("Preencha o campo telefone");
    }
    else {
      console.log('Salvo');
    }
  }
  }


