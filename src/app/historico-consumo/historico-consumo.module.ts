import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HistoricoConsumoPageRoutingModule } from './historico-consumo-routing.module';

import { HistoricoConsumoPage } from './historico-consumo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HistoricoConsumoPageRoutingModule
  ],
  declarations: [HistoricoConsumoPage]
})
export class HistoricoConsumoPageModule {}
