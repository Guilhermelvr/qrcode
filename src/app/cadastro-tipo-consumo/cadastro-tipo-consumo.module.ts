import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CadastroTipoConsumoPageRoutingModule } from './cadastro-tipo-consumo-routing.module';

import { CadastroTipoConsumoPage } from './cadastro-tipo-consumo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CadastroTipoConsumoPageRoutingModule
  ],
  declarations: [CadastroTipoConsumoPage]
})
export class CadastroTipoConsumoPageModule {}
