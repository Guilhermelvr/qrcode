import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cadastro-tipo-consumo',
  templateUrl: './cadastro-tipo-consumo.page.html',
  styleUrls: ['./cadastro-tipo-consumo.page.scss'],
})
export class CadastroTipoConsumoPage implements OnInit {

  public consumos: string[] = ['Prato Feito', 'Sobremesa', 'Bebidas', 'Comida', 'Teste'];

  constructor() { }

  ngOnInit() { }

  public excluir(consumo: string) {
    this.consumos = this.consumos.filter((el) => el != consumo);
  }
}
