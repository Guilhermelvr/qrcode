import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FormRecompensaPage } from './form-recompensa.page';

describe('FormRecompensaPage', () => {
  let component: FormRecompensaPage;
  let fixture: ComponentFixture<FormRecompensaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormRecompensaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FormRecompensaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
