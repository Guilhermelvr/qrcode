import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FormTipoConsumoPageRoutingModule } from './form-tipo-consumo-routing.module';

import { FormTipoConsumoPage } from './form-tipo-consumo.page';
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FormTipoConsumoPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [FormTipoConsumoPage]
})
export class FormTipoConsumoPageModule {}
