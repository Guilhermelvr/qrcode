import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FormTipoConsumoPage } from './form-tipo-consumo.page';

describe('FormTipoConsumoPage', () => {
  let component: FormTipoConsumoPage;
  let fixture: ComponentFixture<FormTipoConsumoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormTipoConsumoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FormTipoConsumoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
