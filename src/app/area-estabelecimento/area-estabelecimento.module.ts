import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AreaEstabelecimentoPageRoutingModule } from './area-estabelecimento-routing.module';

import { AreaEstabelecimentoPage } from './area-estabelecimento.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AreaEstabelecimentoPageRoutingModule
  ],
  declarations: [AreaEstabelecimentoPage]
})
export class AreaEstabelecimentoPageModule {}
