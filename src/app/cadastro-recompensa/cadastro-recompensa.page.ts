import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cadastro-recompensa',
  templateUrl: './cadastro-recompensa.page.html',
  styleUrls: ['./cadastro-recompensa.page.scss'],
})
export class CadastroRecompensaPage implements OnInit {
  public cadastro: string[] = ['Compre 10 Pratos e ganhe outro', 'Compre 10 Sobremessa e ganhe outra', 'Compre 10 Bebidas e ganhe outra'];
  constructor() { }

  public excluir(consumo: string) {
    this.cadastro = this.cadastro.filter((el) => el != consumo);
  }
  ngOnInit() {
  }

}
