import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CadastroRecompensaPageRoutingModule } from './cadastro-recompensa-routing.module';

import { CadastroRecompensaPage } from './cadastro-recompensa.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CadastroRecompensaPageRoutingModule
  ],
  declarations: [CadastroRecompensaPage]
})
export class CadastroRecompensaPageModule {}
