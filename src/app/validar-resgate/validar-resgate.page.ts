import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-validar-resgate',
  templateUrl: './validar-resgate.page.html',
  styleUrls: ['./validar-resgate.page.scss'],
})
export class ValidarResgatePage implements OnInit {

  private usuarios:any;
  private pesquisa:string = '';

  constructor() { }

  ngOnInit() {
    this.usuarios = [
      {
        id: 1,
        email: 'a@b.com',
        telefone: '(41) 92922-2222',
        recompensa: 'Compre 10 pratos e ganhe outro'
      },
      {
        id: 2,
        email: 'joao@teste.com',
        telefone: '(41) 92922-2222',
        recompensa: 'Compre 12 pratos e ganhe outro'
      },
      {
        id: 3,
        email: 'maria@teste.com',
        telefone: '(41) 92922-2222',
        recompensa: 'Compre 32 pratos e ganhe outro'
      },
      {
        id: 4,
        email: 'jose@teste.com',
        telefone: '(41) 92922-3333',
        recompensa: 'Compre 23 pratos e ganhe outro'
      },
    ];
  }

}
