import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ValidarResgatePageRoutingModule } from './validar-resgate-routing.module';

import { ValidarResgatePage } from './validar-resgate.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ValidarResgatePageRoutingModule
  ],
  declarations: [ValidarResgatePage]
})
export class ValidarResgatePageModule {}
