import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ValidarResgatePage } from './validar-resgate.page';

describe('ValidarResgatePage', () => {
  let component: ValidarResgatePage;
  let fixture: ComponentFixture<ValidarResgatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValidarResgatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ValidarResgatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
