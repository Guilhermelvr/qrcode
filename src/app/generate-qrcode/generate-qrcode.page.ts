import { Component, OnInit } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

@Component({
  selector: 'app-generate-qrcode',
  templateUrl: './generate-qrcode.page.html',
  styleUrls: ['./generate-qrcode.page.scss'],
})
export class GenerateQrcodePage{
  qrData = null;
  createdCode = null;
  scannedCode = null;

  constructor(private barcodeScanner : BarcodeScanner){

  }


  createCode(){
  this.createdCode = this.qrData;
  
}
scanCode(){
  this.barcodeScanner.scan().then(barcodeData => {
  this.scannedCode = barcodeData.text;
})

}











}
