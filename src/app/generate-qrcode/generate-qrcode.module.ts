import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GenerateQrcodePageRoutingModule } from './generate-qrcode-routing.module';

import { GenerateQrcodePage } from './generate-qrcode.page';
import { NgxQRCodeModule } from 'ngx-qrcode2'
import { BarcodeScanner} from '@ionic-native/barcode-scanner/ngx'
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GenerateQrcodePageRoutingModule,
    NgxQRCodeModule,
    
  ],
  declarations: [GenerateQrcodePage],
  providers: [
    BarcodeScanner
  ],
})
export class GenerateQrcodePageModule {}
