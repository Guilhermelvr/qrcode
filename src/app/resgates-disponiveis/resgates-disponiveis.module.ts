import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ResgatesDisponiveisPageRoutingModule } from './resgates-disponiveis-routing.module';

import { ResgatesDisponiveisPage } from './resgates-disponiveis.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ResgatesDisponiveisPageRoutingModule
  ],
  declarations: [ResgatesDisponiveisPage]
})
export class ResgatesDisponiveisPageModule {}
