import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ResgatesDisponiveisPage } from './resgates-disponiveis.page';

const routes: Routes = [
  {
    path: '',
    component: ResgatesDisponiveisPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResgatesDisponiveisPageRoutingModule {}
