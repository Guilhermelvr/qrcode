import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BrMaskerModule } from 'br-mask';
import { IonicModule } from '@ionic/angular';

import { MarcarConsumoPageRoutingModule } from './marcar-consumo-routing.module';
import { ReactiveFormsModule } from "@angular/forms";

import { MarcarConsumoPage } from './marcar-consumo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MarcarConsumoPageRoutingModule,
    ReactiveFormsModule,
    BrMaskerModule
  ],
  declarations: [MarcarConsumoPage]
})
export class MarcarConsumoPageModule {}
