import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-marcar-consumo',
  templateUrl: './marcar-consumo.page.html',
  styleUrls: ['./marcar-consumo.page.scss'],
})
export class MarcarConsumoPage {

  public loginForm: any;
  router: any;
  constructor(formBuilder: FormBuilder, public alertController: AlertController) {
    this.loginForm = formBuilder.group({
      numtel: new FormControl('', [Validators.required]),
      conteudo: new FormControl('', [Validators.required]),
      unidades: new FormControl('',[Validators.required])
    });
  
  }
  async login() {
    let { numtel, conteudo, unidades } = this.loginForm.controls;

    if (!this.loginForm.valid) {
    }
    if (!numtel.valid) {
      const alert = await this.alertController.create({
        header: 'Form Consumo',
       
        message: 'Preencha o campo telefone ',
        buttons: ['OK']
      });
      await alert.present();
     

    } else if (!conteudo.valid) {
      const alert = await this.alertController.create({
        header: 'Form Consumo',
       
        message: 'Preencha o campo conteudo ',
        buttons: ['OK']
      });
      await alert.present();
    
    } else if (!unidades.valid) {
      const alert = await this.alertController.create({
        header: 'Form Consumo',
       
        message: 'Informa quantas unidade ',
        buttons: ['OK']
      });
      await alert.present();
    }
    else {
      const alert = await this.alertController.create({
        header: 'Form Consumo',
       
        message: 'Salvo com sucesso',
        buttons: ['OK']
      });
      await alert.present();
    }
  }
}

