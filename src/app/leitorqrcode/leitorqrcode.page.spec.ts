import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LeitorqrcodePage } from './leitorqrcode.page';

describe('LeitorqrcodePage', () => {
  let component: LeitorqrcodePage;
  let fixture: ComponentFixture<LeitorqrcodePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeitorqrcodePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LeitorqrcodePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
