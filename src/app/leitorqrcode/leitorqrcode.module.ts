import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LeitorqrcodePageRoutingModule } from './leitorqrcode-routing.module';

import { LeitorqrcodePage } from './leitorqrcode.page';
import { QRScanner } from "@ionic-native/qr-scanner/ngx"
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LeitorqrcodePageRoutingModule
  ],
  declarations: [LeitorqrcodePage],
  providers: [
    QRScanner
  ],
})
export class LeitorqrcodePageModule {}
