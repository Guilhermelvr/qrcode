import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LeitorqrcodePage } from './leitorqrcode.page';

const routes: Routes = [
  {
    path: '',
    component: LeitorqrcodePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LeitorqrcodePageRoutingModule {}
