import { Component } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  [x: string]: any;
  public loginForm: any;
  router: any;
  constructor(formBuilder: FormBuilder, router: Router, public alertController: AlertController) {
    this.loginForm = formBuilder.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.minLength(6), Validators.maxLength(20), Validators.required]),
    });
    this.router = router;
  }
  async login() {
    let { email, password } = this.loginForm.controls;

    if (!this.loginForm.valid) {
    }
    if (!email.valid) {
      const alert = await this.alertController.create({
        header: 'Cadastro Cliente',
        
        message: '"Email invalido ou campo vazio"',
        buttons: ['OK']
      });
      await alert.present();

    } else if (!password.valid) {
      const alert = await this.alertController.create({
        header: 'Cadastro Cliente',
        
        message: 'Senha invalida ou campo vazio',
        buttons: ['OK']
      });
  
      await alert.present();
    }
    else {
      this.router.navigate(['/', 'area-estabelecimento']);
    }
  }
}
